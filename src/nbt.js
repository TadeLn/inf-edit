const NBT = JSON.parse(`{
    "TAG_End": 0,
    "TAG_Byte": 1,
    "TAG_Short": 2,
    "TAG_Int": 3,
    "TAG_Long": 4,
    "TAG_Float": 5,
    "TAG_Double": 6,
    "TAG_Byte_Array": 7,
    "TAG_String": 8,
    "TAG_List": 9,
    "TAG_Compound": 10,
    "TAG_Int_Array": 11,
    "TAG_Long_Array": 12
}`);

function parseTagByte(offset) {
    let number = rawdata[offset];
    if (number >= 128) {
        number -= 256;
    }
    return {data: number, size: 1};
}

function parseTagShort(offset) {
    let number = (rawdata[offset] * 256) + rawdata[offset+1];
    if (number >= 32768) {
        number -= 65536;
    }
    return {data: number, size: 2};
}

function parseTagInt(offset) {
    let number = (rawdata[offset] * 16777216)+(rawdata[offset+1] * 65536)+(rawdata[offset+2] * 256)+(rawdata[offset+3]);
    if (number >= 2147483648) {
        number -= 4294967296;
    }
    return {data: number, size: 4};
}

function parseTagLong(offset) {
    let number = 0;
    for (let i = 0; i < 8; i++) {
        number += rawdata[offset+i] * Math.pow(256, 7-i);
    }
    if (number >= Math.pow(256, 8)/2) {
        number -= Math.pow(256, 8);
    }
    return {data: number, size: 8};
}

function parseTagFloat(offset) {
    let buffer = new ArrayBuffer(4);
    let bytes = new Int8Array(buffer);
    for (let i = 0; i < 4; i++) {
        bytes[i] = rawdata[offset+i];
    }
    bytes.reverse();
    let number = new Float32Array(buffer)[0];
    return {data: number, size: 4};
}

function parseTagDouble(offset) {
    let buffer = new ArrayBuffer(8);
    let bytes = new Int8Array(buffer);
    for (let i = 0; i < 8; i++) {
        bytes[i] = rawdata[offset+i];
    }
    bytes.reverse();
    let number = new Float64Array(buffer)[0];
    return {data: number, size: 8};
}

function parseTagByteArray(offset) {
    let result = [];
    let length = (rawdata[offset] * 16777216)+(rawdata[offset+1] * 65536)+(rawdata[offset+2] * 256)+(rawdata[offset+3]);
    if (length >= 2147483648 || length == 0) {
        length = 0;
    }
    let ptr;
    for (ptr = 4; ptr < length+4; ptr++) {
        let number = rawdata[offset+ptr];
        if (number >= 128) {
            number -= 256;
        }
        result.push(number);
    }
    return {data: result, size: ptr};
}

function parseTagString(offset) {
    let length = (rawdata[offset] * 256) + rawdata[offset+1];
    let ptr;
    let result = "";
    for (ptr = 2; ptr < length+2; ptr++) {
        result += String.fromCharCode(rawdata[offset+ptr]);
    }
    return {data: result, size: ptr};
}

function parseTagList(offset) {
    let result = [];
    let type = rawdata[offset];
    let ptr = 5;
    let length = (rawdata[offset+1] * 16777216)+(rawdata[offset+2] * 65536)+(rawdata[offset+3] * 256)+(rawdata[offset+4]);
    if (length >= 2147483648 || length == 0) {
        length = 0;
    }
    if (length != 0 && (type == NBT.TAG_End || type > NBT.TAG_Long_Array)) {
        console.log("Invalid List Type: " + type);
        return undefined;
    }
    for (let i = 0; i < length; i++) {
        
        let temp_tag;
        switch (type) {
            /*
            case NBT.TAG_End:
                break;
            */

            case NBT.TAG_Byte:
                temp_tag = parseTagByte(offset + ptr);
                break;

            case NBT.TAG_Short:
                temp_tag = parseTagShort(offset + ptr);
                break;

            case NBT.TAG_Int:
                temp_tag = parseTagInt(offset + ptr);
                break;

            case NBT.TAG_Long:
                temp_tag = parseTagLong(offset + ptr);
                break;

            case NBT.TAG_Float:
                temp_tag = parseTagFloat(offset + ptr);
                break;
                
            case NBT.TAG_Double:
                temp_tag = parseTagDouble(offset + ptr);
                break;

            case NBT.TAG_Byte_Array:
                temp_tag = parseTagByteArray(offset + ptr);
                break;

            case NBT.TAG_String:
                temp_tag = parseTagString(offset + ptr);
                break;

            case NBT.TAG_List:
                temp_tag = parseTagList(offset + ptr);
                break;

            case NBT.TAG_Compound:
                temp_tag = parseTagCompound(offset + ptr);
                break;

            case NBT.TAG_Int_Array:
                temp_tag = parseTagIntArray(offset + ptr);
                break;

            case NBT.TAG_Long_Array:
                temp_tag = parseTagLongArray(offset + ptr);
                break;

            default:
                console.log("Unknown Tag: " + rawdata[ptr]);
                temp_tag = undefined;
                break;
        }
        
        if (temp_tag == undefined) break;
        ptr += temp_tag.size;
        result.push(temp_tag.data);
    }
    return {data: result, size: ptr, type: type};
}

function parseTagCompound(offset) {
    let result = {};
    let ptr = 0;

    while (true) {
        let type = rawdata[offset+ptr];
        if (type == NBT.TAG_End || type === undefined) {
            ptr++;
            return {data: result, size: ptr};
        }

        let nameLength = (rawdata[offset+ptr+1] * 256) + rawdata[offset+ptr+2];
        let name = "";
        for (let i = 0; i < nameLength; i++) {
            name += String.fromCharCode(rawdata[offset+ptr+3+i]);
        }
        ptr += 3 + nameLength;

        let temp_tag;
        switch (type) {
            /*
            case NBT.TAG_End:
                break;
            */

            case NBT.TAG_Byte:
                temp_tag = parseTagByte(offset + ptr);
                break;

            case NBT.TAG_Short:
                temp_tag = parseTagShort(offset + ptr);
                break;

            case NBT.TAG_Int:
                temp_tag = parseTagInt(offset + ptr);
                break;

            case NBT.TAG_Long:
                temp_tag = parseTagLong(offset + ptr);
                break;

            case NBT.TAG_Float:
                temp_tag = parseTagFloat(offset + ptr);
                break;
                
            case NBT.TAG_Double:
                temp_tag = parseTagDouble(offset + ptr);
                break;

            case NBT.TAG_Byte_Array:
                temp_tag = parseTagByteArray(offset + ptr);
                break;

            case NBT.TAG_String:
                temp_tag = parseTagString(offset + ptr);
                break;

            case NBT.TAG_List:
                temp_tag = parseTagList(offset + ptr);
                break;

            case NBT.TAG_Compound:
                temp_tag = parseTagCompound(offset + ptr);
                break;

            case NBT.TAG_Int_Array:
                temp_tag = parseTagIntArray(offset + ptr);
                break;

            case NBT.TAG_Long_Array:
                temp_tag = parseTagLongArray(offset + ptr);
                break;

            default:
                console.log("Unknown Tag: " + rawdata[ptr]);
                temp_tag = undefined;
                throw "Unknown Tag";
                break;
        }

        if (temp_tag == undefined) break;
        ptr += temp_tag.size;
        result[name] = {type: type, data: temp_tag.data};
        if (result[name].type == NBT.TAG_List) result[name].listType = temp_tag.type;
    }
}

function parseTagIntArray(offset) {
    let result = [];
    let length = (rawdata[offset] * 16777216)+(rawdata[offset+1] * 65536)+(rawdata[offset+2] * 256)+(rawdata[offset+3]);
    if (length >= 2147483648 || length == 0) {
        length = 0;
    }
    let ptr;
    for (ptr = 4; ptr < (length*4)+4; ptr += 4) {
        let number = (rawdata[offset+ptr] * 16777216)+(rawdata[offset+ptr+1] * 65536)+(rawdata[offset+ptr+2] * 256)+(rawdata[offset+ptr+3]);
        if (number >= 2147483648) {
            number -= 4294967296;
        }
        result.push(number);
    }
    return {data: result, size: ptr};
}

function parseTagLongArray(offset) {
    let result = [];
    let length = (rawdata[offset] * 16777216)+(rawdata[offset+1] * 65536)+(rawdata[offset+2] * 256)+(rawdata[offset+3]);
    if (length >= 2147483648 || length == 0) {
        length = 0;
    }
    let ptr;
    for (ptr = 4; ptr < (length*8)+4; ptr += 8) {
        let number = 0;
        for (let i = 0; i < 8; i++) {
            number += rawdata[offset+ptr+i] * Math.pow(256, 7-i);
        }
        if (number >= Math.pow(256, 8)/2) {
            number -= Math.pow(256, 8);
        }
        result.push(number);
    }
    return {data: result, size: ptr};
}










function binTagByte(data) {
    let rawdata = new Int8Array(1);
    let byte = data;
    rawdata[0] = byte;
    return rawdata;
}

function binTagShort(data) {
    let rawdata = new Int8Array(2);
    let shortArr = new Int16Array([data]);
    let byteArr = new Int8Array(shortArr.buffer);
    for (let i = 0; i < 2; i++) {
        rawdata[i] = byteArr[i];
    }
    rawdata.reverse();
    return rawdata;
}

function binTagInt(data) {
    let rawdata = new Int8Array(4);
    let intArr = new Int32Array([data]);
    let byteArr = new Int8Array(intArr.buffer);
    for (let i = 0; i < 4; i++) {
        rawdata[i] = byteArr[i];
    }
    rawdata.reverse();
    return rawdata;
}

function binTagLong(data) {
    let rawdata = new Int8Array(8);
    let longArr = new BigUint64Array([BigInt(data)]);
    let byteArr = new Int8Array(longArr.buffer);
    for (let i = 0; i < 8; i++) {
        rawdata[i] = byteArr[i];
    }
    rawdata.reverse();
    return rawdata;
}

function binTagFloat(data) {
    let rawdata = new Int8Array(4);
    let floatArr = new Float32Array([data]);
    let byteArr = new Int8Array(floatArr.buffer);
    for (let i = 0; i < 4; i++) {
        rawdata[i] = byteArr[i];
    }
    rawdata.reverse();
    return rawdata;
}

function binTagDouble(data) {
    let rawdata = new Int8Array(8);
    let doubleArr = new Float64Array([data]);
    let byteArr = new Int8Array(doubleArr.buffer);
    for (let i = 0; i < 8; i++) {
        rawdata[i] = byteArr[i];
    }
    rawdata.reverse();
    return rawdata;
}

function binTagByteArray(data) {
    let rawdata = new Int8Array(4 + data.length);
    let intArr = new Int32Array([data.length]);
    let byteArr = new Int8Array(intArr.buffer);
    for (let i = 0; i < 4; i++) {
        rawdata[3-i] = byteArr[i];
    }
    for (let i = 0; i < data.length; i++) {
        rawdata[i+4] = data[i];
    }
    return rawdata;
}

function binTagString(data) {
    let rawdata = new Int8Array(2 + data.length);
    let shortArr = new Int16Array([data.length]);
    let byteArr = new Int8Array(shortArr.buffer);
    rawdata[1] = byteArr[0];
    rawdata[0] = byteArr[1];
    for (let i = 0; i < data.length; i++) {
        rawdata[i+2] = data.charCodeAt(i);
    }
    return rawdata;
}

function binTagList(data, listType = undefined) {
    let rawdata = [];

    // Store list type
    let type = listType
    rawdata[0] = type;

    // Store list length
    let intArr = new Int32Array([data.length]);
    let byteArr = new Int8Array(intArr.buffer);
    for (let i = 0; i < 4; i++) {
        rawdata[4-i] = byteArr[i];
    }


    // Store contents
    for (let i = 0; i < data.length; i++) {

        let temp_bytes;
        switch (type) {
            /*
            case NBT.TAG_End:
                break;
            */

            case NBT.TAG_Byte:
                temp_bytes = binTagByte(data[i]);
                break;

            case NBT.TAG_Short:
                temp_bytes = binTagShort(data[i]);
                break;

            case NBT.TAG_Int:
                temp_bytes = binTagInt(data[i]);
                break;

            case NBT.TAG_Long:
                temp_bytes = binTagLong(data[i]);
                break;

            case NBT.TAG_Float:
                temp_bytes = binTagFloat(data[i]);
                break;
                
            case NBT.TAG_Double:
                temp_bytes = binTagDouble(data[i]);
                break;

            case NBT.TAG_Byte_Array:
                temp_bytes = binTagByteArray(data[i]);
                break;

            case NBT.TAG_String:
                temp_bytes = binTagString(data[i]);
                break;

            case NBT.TAG_List:
                temp_bytes = binTagList(data[i]);
                break;

            case NBT.TAG_Compound:
                temp_bytes = binTagCompound(data[i]);
                break;

            case NBT.TAG_Int_Array:
                temp_bytes = binTagIntArray(data[i]);
                break;

            case NBT.TAG_Long_Array:
                temp_bytes = binTagLongArray(data[i]);
                break;

            default:
                console.log("Unknown Tag: " + type);
                temp_bytes = undefined;
                break;
        }
        rawdata = rawdata.concat(Array.from(temp_bytes));
    }
    return new Int8Array(rawdata);
}

function binTagCompound(data) {
    let rawdata = [];

    for (let i of Object.keys(data)) {
        let temp_bytes = [];

        // Store type
        temp_bytes[0] = data[i].type;

        // Store name length
        let shortArr = new Int16Array([i.length]);
        let byteArr = new Int8Array(shortArr.buffer);
        temp_bytes[1] = byteArr[1];
        temp_bytes[2] = byteArr[0];

        // Store name
        for (let j = 0; j < i.length; j++) {
            temp_bytes.push(i.charCodeAt(j));
        }

        // Store contents
        switch (data[i].type) {
            /*
            case NBT.TAG_End:
                break;
            */

            case NBT.TAG_Byte:
                temp_bytes = temp_bytes.concat(Array.from(binTagByte(data[i].data)));
                break;

            case NBT.TAG_Short:
                temp_bytes = temp_bytes.concat(Array.from(binTagShort(data[i].data)));
                break;

            case NBT.TAG_Int:
                temp_bytes = temp_bytes.concat(Array.from(binTagInt(data[i].data)));
                break;

            case NBT.TAG_Long:
                temp_bytes = temp_bytes.concat(Array.from(binTagLong(data[i].data)));
                break;

            case NBT.TAG_Float:
                temp_bytes = temp_bytes.concat(Array.from(binTagFloat(data[i].data)));
                break;
                
            case NBT.TAG_Double:
                temp_bytes = temp_bytes.concat(Array.from(binTagDouble(data[i].data)));
                break;

            case NBT.TAG_Byte_Array:
                temp_bytes = temp_bytes.concat(Array.from(binTagByteArray(data[i].data)));
                break;

            case NBT.TAG_String:
                temp_bytes = temp_bytes.concat(Array.from(binTagString(data[i].data)));
                break;

            case NBT.TAG_List:
                temp_bytes = temp_bytes.concat(Array.from(binTagList(data[i].data, data[i].listType)));
                break;

            case NBT.TAG_Compound:
                temp_bytes = temp_bytes.concat(Array.from(binTagCompound(data[i].data)));
                break;

            case NBT.TAG_Int_Array:
                temp_bytes = temp_bytes.concat(Array.from(binTagIntArray(data[i].data)));
                break;

            case NBT.TAG_Long_Array:
                temp_bytes = temp_bytes.concat(Array.from(binTagLongArray(data[i].data)));
                break;

            default:
                console.log("Unknown Tag: " + data[i].type);
                break;
        }
        rawdata = rawdata.concat(Array.from(temp_bytes));
    }
    rawdata.push(NBT.TAG_End);
    return new Int8Array(rawdata);
}

function binTagIntArray(data) {
    let rawdata = new Int8Array(4 + (data.length * 4));
    let intArr = new Int32Array([data.length]);
    let byteArr = new Int8Array(intArr.buffer);
    for (let i = 0; i < 4; i++) {
        rawdata[3-i] = byteArr[i];
    }
    for (let i = 0; i < data.length; i++) {
        let intArr = new Int32Array([data.length]);
        let byteArr = new Int8Array(intArr.buffer);
        for (let j = 0; j < 4; j++) {
            rawdata[(i*4)+4+j] = byteArr[j];
        }
    }
    return rawdata;
}

function binTagLongArray(data) {
    let rawdata = new Int8Array(4 + (data.length * 8));
    let intArr = new Int32Array([data.length]);
    let byteArr = new Int8Array(intArr.buffer);
    for (let i = 0; i < 4; i++) {
        rawdata[3-i] = byteArr[i];
    }
    for (let i = 0; i < data.length; i++) {
        let longArr = new BigInt64Array([BigInt(data.length)]);
        let byteArr = new Int8Array(intArr.buffer);
        for (let j = 0; j < 8; j++) {
            rawdata[(i*8)+4+j] = byteArr[j];
        }
    }
    return rawdata;
}