const { ipcRenderer, shell } = require("electron");
const path = require('path');
const fs = require("fs");
const zlib = require('zlib');

const MCVer = [
    "rd-132211",
    "rd-160052",
    "rd-161348",
    "c0.0.13",
    "inf-20100618",
    "a1.0.4"
];

const itemData = JSON.parse(`[
    {
        "0": ["Air", "crash"],
        "1": ["Cobblestone", "1-0"]
    },
    {
        "1": ["Stone"],
        "2": ["Grass Block"],
        "3": ["Dirt"],
        "4": ["Cobblestone"],
        "5": ["Wooden Planks", "5-0"]
    },
    {
        "5": ["Wooden Planks"],
        "6": ["Sapling", "6-0"]
    },
    {
        "6": ["Sapling"],
        "7": ["Bedrock"],
        "8": ["Water"],
        "9": ["Water (Still)", "8"],
        "10": ["Lava"],
        "11": ["Lava (Still)", "10"]
    },
    {
        "12": ["Sand"],
        "13": ["Gravel"],
        "14": ["Gold Ore"],
        "15": ["Iron Ore"],
        "16": ["Coal Ore"],
        "17": ["Wood"],
        "18": ["Leaves"],
        "19": ["Sponge"],
        "20": ["Glass"],
        "21": ["Red Cloth"],
        "22": ["Orange Cloth"],
        "23": ["Yellow Cloth"],
        "24": ["Chartreuse Cloth"],
        "25": ["Green Cloth"],
        "26": ["Spring Green Cloth"],
        "27": ["Cyan Cloth"],
        "28": ["Capri Cloth"],
        "29": ["Ultramarine Cloth"],
        "30": ["Violet Cloth"],
        "31": ["Purple Cloth"],
        "32": ["Magenta Cloth"],
        "33": ["Rose Cloth"],
        "34": ["Gray Cloth"],
        "35": ["Light Gray Cloth"],
        "36": ["White Cloth"],
        "37": ["Yellow Flower"],
        "38": ["Rose"],
        "39": ["Brown Mushroom"],
        "40": ["Red Mushroom"],
        "41": ["Block of Gold"],
        "42": ["Block of Iron"],
        "43": ["Double Stone Slab"],
        "44": ["Stone Slab"],
        "45": ["Bricks"],
        "46": ["TNT"],
        "47": ["Bookshelf"],
        "48": ["Mossy Cobblestone"],
        "49": ["Obsidian"],
        "50": ["Torch"],
        "51": ["Fire"],
        "52": ["Water Source Block", "52-0"],
        "53": ["Lava Source Block", "53-0"],
        "54": ["Chest"],
        "55": ["Gear"],
        "56": ["Diamond Ore"],
        "57": ["Block of Diamond"],
        "58": ["Crafting Table"],
        "59": ["Wheat (Block)"],
        "60": ["Soil"],
        "61": ["Furnace"],
        "62": ["Lit Furnace"],
        "63": ["Sign (Block)"],
        "64": ["Door (Block)"],
        "65": ["Ladder"],
        "66": ["Rail"],

        "256": ["Iron Shovel"],
        "257": ["Iron Pickaxe"],
        "258": ["Iron Axe"],
        "259": ["Flint and Steel"],
        "260": ["Apple"],
        "261": ["Bow"],
        "262": ["Arrow"],
        "263": ["Coal"],
        "264": ["Diamond"],
        "265": ["Iron Bar"],
        "266": ["Gold Bar"],
        "267": ["Iron Sword"],
        "268": ["Wooden Sword"],
        "269": ["Wooden Shovel"],
        "270": ["Wooden Pickaxe"],
        "271": ["Wooden Axe"],
        "272": ["Stone Sword"],
        "273": ["Stone Shovel"],
        "274": ["Stone Pickaxe"],
        "275": ["Stone Axe"],
        "276": ["Diamond Sword"],
        "277": ["Diamond Shovel"],
        "278": ["Diamond Pickaxe"],
        "279": ["Diamond Axe"],
        "280": ["Stick"],
        "281": ["Bowl"],
        "282": ["Mushroom Soup"],
        "283": ["Golden Sword"],
        "284": ["Golden Shovel"],
        "285": ["Golden Pickaxe"],
        "286": ["Golden Axe"],
        "287": ["String"],
        "288": ["Feather"],
        "289": ["Gunpowder"],
        "290": ["Wooden Hoe"],
        "291": ["Stone Hoe"],
        "292": ["Iron Hoe"],
        "293": ["Diamond Hoe"],
        "294": ["Golden Hoe"],
        "295": ["Seeds"],
        "296": ["Wheat"],
        "297": ["Bread"],
        "298": ["Cloth Helmet"],
        "299": ["Cloth Chestplate"],
        "300": ["Cloth Leggings"],
        "301": ["Cloth Boots"],
        "302": ["Chain Helmet"],
        "303": ["Chain Chestplate"],
        "304": ["Chain Leggings"],
        "305": ["Chain Boots"],
        "306": ["Iron Helmet"],
        "307": ["Iron Chestplate"],
        "308": ["Iron Leggings"],
        "309": ["Iron Boots"],
        "310": ["Diamond Helmet"],
        "311": ["Diamond Chestplate"],
        "312": ["Diamond Leggings"],
        "313": ["Diamond Boots"],
        "314": ["Golden Helmet"],
        "315": ["Golden Chestplate"],
        "316": ["Golden Leggings"],
        "317": ["Golden Boots"],
        "318": ["Flint"],
        "319": ["Raw Porkchop"],
        "320": ["Cooked Porkchop"],
        "321": ["Painting"],
        "322": ["Golden Apple"],
        "323": ["Sign"],
        "324": ["Door"],
        "325": ["Bucket"],
        "326": ["Water Bucket"],
        "327": ["Lava Bucket"],
        "328": ["Minecart", "328-0"]
    },
    {
        "21": ["Red Cloth", "crash"],
        "22": ["Orange Cloth", "crash"],
        "23": ["Yellow Cloth", "crash"],
        "24": ["Chartreuse Cloth", "crash"],
        "25": ["Green Cloth", "crash"],
        "26": ["Spring Green Cloth", "crash"],
        "27": ["Cyan Cloth", "crash"],
        "28": ["Capri Cloth", "crash"],
        "29": ["Ultramarine Cloth", "crash"],
        "30": ["Violet Cloth", "crash"],
        "31": ["Purple Cloth", "crash"],
        "32": ["Magenta Cloth", "crash"],
        "33": ["Rose Cloth", "crash"],
        "34": ["Gray Cloth", "crash"],
        "35": ["White Cloth", "36"],
        "36": ["White Cloth", "crash"],

        "52": ["Spawner"],
        "53": ["Wooden Stairs"],

        "55": ["Redstone Dust (Block)"],

        "67": ["Cobblestone Stairs"],
        "68": ["Wall Sign (Block)"],
        "69": ["Lever"],
        "70": ["Stone Pressure Plate"],
        "71": ["Iron Door (Block)"],
        "72": ["Wooden Pressure Plate"],
        "73": ["Redstone Ore"],
        "74": ["Lit Redstone Ore"],
        "75": ["Unlit Redstone Torch"],
        "76": ["Redstone Torch"],
        "77": ["Stone Button"],
        "78": ["Snow Layer"],
        "79": ["Ice"],

        "328": ["Minecart"],
        "329": ["Saddle"],
        "330": ["Iron Door"],
        "331": ["Redstone Dust"]
    }
]`)

let defaultVersion = MCVer[4]; // Load from config
let defaultTheme = "dark"; // Load from config

let rawdata = new Uint8Array();
let mainFile = {};
let fileLoaded = false;

let mode = "";
let screen = "";
let version = defaultVersion;

function versionNumber(ver = version) {
    return MCVer.findIndex((x) => {return x == ver});
}

function getDataFromPath(path, data = -1) {
    if (mainFile.root === undefined) return;
    if (data === -1) data = mainFile.root.data
    let result = data;
    for (x of path) {
        if (result[x] === undefined) {
            if (result[parseInt(x)] === undefined) {
                result = result[parseInt(x)];
            } else {
                return undefined;
            }
        }
        else if (result[x].data === undefined) { result = result[x]; }
        else {
            result = result[x].data;
        }
    }
    return result;
}

function getTypeFromPath(path, data = -1) {
    if (mainFile.root === undefined) return;
    if (data === -1) data = mainFile.root.data
    let result = data;
    let type = 0;
    for (x of path) {
        if (result[x] === undefined) { return undefined; }
        else if (result[x].data === undefined) { result = result[x]; }
        else {
            type = result[x].type;
            result = result[x].data;
        }
    }
    return type;
}

function setDataAtPath(path, value, data = mainFile.root) {
    // Current data and type of that data
    let currentData = data.data;
    let currentType = data.type;

    // Reference to the object that holds currentData and currentType
    let objectRef = data;

    // Type of the list that currentData is in
    let listType = -1;

    // Check if array is the correct type
    if (typeof path !== "object") return -1;

    // Delete mode
    let deleteMode = (value === undefined);

    // Find the desired variable
    for (let i = 0; i < path.length; i++) {

        let pathSegment = path[i];
        let theEnd = false;

        // Update currentData and currentType
        switch (currentType) {

            // If it's a list
            case NBT.TAG_List:
            case NBT.TAG_Byte_Array:
            case NBT.TAG_Int_Array:
            case NBT.TAG_Long_Array:
                // Update objectRef
                objectRef = currentData;

                // Update currentType and currentData
                currentType = listType;
                currentData = currentData[pathSegment];
                break;

            // If it's a compound
            case NBT.TAG_Compound:
                // Update currentType
                currentType = currentData[pathSegment].type;

                // If the new value is a list or some sort of an array, store its type to listType
                if (currentType == NBT.TAG_List) { listType = currentData[pathSegment].listType; }
                else if (currentType == NBT.TAG_Byte_Array) { listType = NBT.TAG_Byte; }
                else if (currentType == NBT.TAG_Int_Array) { listType = NBT.TAG_Int; }
                else if (currentType == NBT.TAG_Long_Array) { listType = NBT.TAG_Long; }
                // If the new value is not a list nor an array, store -1 to listType
                else { listType = -1 }

                // If its the last element and deleteMode is enabled
                if (i == path.length-1 && deleteMode) {
                    // Don't update objectRef
                } else {
                    // Update objectRef
                    objectRef = currentData[pathSegment];
                }

                // Update currentData
                currentData = currentData[pathSegment].data;
                break;

            default:
                // If you can't go inside of the value, stop
                theEnd = true;
                break;
        }
        if (theEnd) break;
    }

    // If objectRef is an array
    if (objectRef.data === undefined) {
        // If deleteMode is enabled
        if (deleteMode) {
            // Delete the value
            objectRef.splice(path[path.length-1], 1);
        } else {
            // Set the correct element of the array to the value
            objectRef[path[path.length-1]] = value;
        }
    }
    // If objectRef is an object
    else {
        // If deleteMode is enabled
        if (deleteMode) {
            // Delete the value
            delete objectRef.data[path[path.length-1]];
        } else {
            // Set the correct element of the array to the value
            objectRef.data = value;
        }
    }
}



function getItemData(id) {
    result = ["Crashing item", "crash"];
    for (let i = 0; i <= versionNumber(); i++) {
        if (itemData[i].hasOwnProperty(id)) {
            result = itemData[i][id]; 
        }
    }
    if (result[1] === undefined) {
        result[1] = id;
    }
    return result;
}



ipcRenderer.on("load_start", (event) => {
    load_start();
});

function load(path = "") {
    changeVersionScreen();
    if (path == "") {
        path = mainFile.path;
    }

    mainFile.path = path;
}

function loadData() {
    // Load from file
    let path = mainFile.path;
    rawdata = fs.readFileSync(path);

    let isCompressed = false;
    // Detect compressed data
    if (rawdata[0] == 0x1f && rawdata[1] == 0x8b) {
        isCompressed = true;

        // Decompress data
        rawdata = zlib.gunzipSync(Buffer.from(rawdata));
    }

    // Parse NBT data
    try {
        mainFile.root = parseTagCompound(0);
        mainFile.root.type = 10;
    } catch (e) {
        console.log(e);
    }
    if (mainFile === undefined) {mainFile = {};}
    mainFile.compressed = isCompressed;
    mainFile.path = path;

    // Print raw data
    let result = "";
    for (let i = 0; i < rawdata.length; i++) {
        result += rawdata[i] + " ";
        if (i % 16 == 15) {
            result += "\n";
        }
    }
    document.getElementById("fileContents").innerHTML = result;
    document.getElementById("tree").innerHTML = "";
    renderTree([], document.getElementById("tree"));
    renderInventory();
    fileLoaded = true;

    screen = "";
    updateScreens();
}

ipcRenderer.on("loadFile_start", (event) => {
    loadFileBtn();
});

function loadFileBtn() {
    ipcRenderer.send("loadFileDialog_start");
}

ipcRenderer.on("loadFileDialog_return", (event, path) => {
    if (path != "") load(path)
});


ipcRenderer.on("save_start", (event) => {
    save();
});

function save(path = "") {
    // Save to file
    rawdata = new Int8Array(binTagCompound(mainFile.root.data));

    if (path == "") {path = mainFile.path}
    let bytes = Buffer.from(rawdata);
    if (mainFile.compressed) {
        bytes = zlib.gzipSync(bytes);
    }
    fs.writeFileSync(path, bytes);
}


ipcRenderer.on("saveAs_start", (event) => {
    saveAsBtn();
});

function saveAsBtn() {
    ipcRenderer.send("saveFileDialog_start");
}

ipcRenderer.on("saveFileDialog_return", (event, path) => {
    if (path != "") save(path);
});


ipcRenderer.on("exportFile_start", (event, type) => {
    exportBtn(type);
});

function exportBtn(type = 1) {
    ipcRenderer.send("exportFileDialog_start", type);
}

ipcRenderer.on("exportFileDialog_return", (event, path, type) => {
    // Export to file
    console.log("EXPORT (" + type + ") TO: " + path);
});


ipcRenderer.on("changeMode", (event, newMode) => {
    updateMode(newMode);
});


function updateChunkList() {
    mainFile.chunkList = [];
    let worldDirPath = path.dirname(mainFile.path);

    fs.readdir(worldDirPath, {withFileTypes: true}, (err, directories) => {
        for (let dir of directories) {
            if (!dir.isDirectory()) continue;
            let dirPath = worldDirPath + path.sep + dir.name;
            let subdirectories = fs.readdirSync(dirPath, {withFileTypes: true});
            for (let subdir of subdirectories) {
                if (!subdir.isDirectory()) continue;
                let subdirPath = dirPath + path.sep + subdir.name;
                let chunkFiles = fs.readdirSync(subdirPath, {withFileTypes: true});
                for (let file of chunkFiles) {
                    if (!file.isFile()) continue;
                    let filePath = subdirPath + path.sep + file.name;
                    mainFile.chunkList.push(filePath);
                }
            }
        }
        console.log("Done updating chunk list!");
    });
}





function updateMode(newMode) {
    // If newMode is not diffrent, don't do anything
    if (newMode === mode) return;

    // Change mode to new mode
    mode = newMode;

    // Loop through all divs with class mode
    for (let elem of document.getElementsByClassName("mode")) {
        // Remove class "active"
        elem.classList.remove("active");

        // If this div is the selected mode, then add class "active"
        if (elem.id == "mode_" + mode) {
            elem.classList.add("active");
        }
    }

    // Loop through all divs with class mode
    for (let elem of document.getElementsByClassName("menuItem")) {
        // Remove class "active"
        elem.classList.remove("active");

        // If this div is the selected mode, then add class "active"
        if (elem.id == "menuItem_" + mode) {
            elem.classList.add("active");
        }
    }

    // Focus on body / deselect all input fields
    document.body.focus();

    // Refresh inventory
    if (mode == "inventory") {
        renderInventory();
    }

    // Update screens
    updateScreens();
}

function changeVersion(newVersion) {
    // Update version variable
    version = newVersion;

    // Refresh inventory if needed
    if (mode == "inventory") {
        renderInventory();
    }

    // Update item list
    let elem = document.getElementById("itemDetails_id")
    elem.innerHTML = "";

    let header = document.createElement("div");
    header.classList.add("header");
    header.addEventListener("click", () => {
        if (header.parentElement.hasAttribute("active")) {
            header.parentElement.removeAttribute("active");
        } else {
            header.parentElement.setAttribute("active", true);
        }
        saveSlot();
    });
    elem.appendChild(header);    

    let ul = document.createElement("ul");
    ul.setAttribute("value", 0);
    ul.addEventListener("mouseleave", () => {
        header.parentElement.removeAttribute("active");
        saveSlot();
    });

    for (let id = 0; id < 512; id++) {
        let li = document.createElement("li");
        li.value = id;

            icon = document.createElement("div");
            icon.classList.add("icon");
            icon.style.backgroundImage = "url('../assets/img/inv/item/" + getItemData(id)[1] + ".png')";
            li.appendChild(icon);
        
        li.addEventListener("click", () => {
            li.parentElement.children[li.parentElement.getAttribute("value")].removeAttribute("selected");
            li.parentElement.setAttribute("value", li.getAttribute("value"));
            li.parentElement.parentElement.getElementsByClassName("header")[0].innerHTML = li.innerHTML;
            li.setAttribute("selected", true);
        })
        li.innerHTML += id + `: ` + getItemData(id)[0];
        ul.appendChild(li);
    }

    elem.appendChild(ul);

    ul.children[0].click();

    elem.addEventListener("click", () => {
        elem.setAttribute("value", elem.children[1].getAttribute("value"));
    });
}

function updateScreens() {
    if (screen !== "changeVersion") {
        screen =  "";
        if (mode == "inventory" || mode == "worldSettings") {
            if (versionNumber() < versionNumber("inf-20100618")) {
                screen = "modeNotAvaliable";
            }
        }

        if (!fileLoaded) {
            if (mode == "tree"
            || mode == "inventory"
            || mode == "world"
            || mode == "worldSettings") {
                screen = "fileNotLoaded";
            }
        }
    }

    for (let elem of document.getElementsByClassName("screen")) {
        elem.style.visibility = "hidden";
        if (elem.id == "screen_" + screen) {
            elem.style.visibility = "visible";
        }
    }
}


ipcRenderer.on("toggleFullscreen", (event) => {
    toggleFullscreen();
});

function toggleFullscreen() {
    if (document.fullscreenElement === null) {
        document.body.requestFullscreen();
        //document.getElementById("fullscreenButton").classList.add("active");
    } else {
        document.exitFullscreen();
        //document.getElementById("fullscreenButton").classList.remove("active");
    }
}


ipcRenderer.on("zoom", (event, a) => {
    zoom(a);
});

function zoom(magnitude) {
    document.body.style.zoom = magnitude;
}


ipcRenderer.on("newWindow", (event) => {
    newWindow();
});

function newWindow() {
    let link = document.createElement("a");
    link.href = "";
    link.target = "_blank";
    document.body.appendChild(link);
    link.focus();
    link.click();
    link.remove();
}


ipcRenderer.on("changeVersion", (event) => {
    changeVersionScreen();
});

function changeVersionScreen() {
    screen = "changeVersion";
    updateScreens();
}



let treeId = 0;
function renderTree(path, elem, listType = -1) {
    // Get data
    let data = getDataFromPath(path);

    // Create a list of all elements
    let list = document.createElement("ul");

    for (let x of Object.keys(data)) {
        let newPath = path.concat([x]);

        // Create a list item
        let listElem = document.createElement("li");
        listElem.classList.add("treeBranch");

            if (data[x].type !== undefined) {
                // Icon
                let nbtIcon = document.createElement("div");
                nbtIcon.classList.add("nbtIcon");
                nbtIcon.style.backgroundImage = "url('../assets/img/nbt/type" + data[x].type + ".png')";
                listElem.appendChild(nbtIcon);

                // Icon 2 (for lists)
                if (data[x].type == NBT.TAG_List) {
                    let nbtListIcon = document.createElement("div");
                    nbtListIcon.classList.add("nbtIcon");
                    nbtListIcon.style.backgroundImage = "url('../assets/img/nbt/type" + data[x].listType + ".png')";
                    listElem.appendChild(nbtListIcon);
                }
            }

            // Name
            let elemName = document.createElement("b");
            elemName.innerText = x;
            listElem.appendChild(elemName);

            // Colon (between name and value)
            let colon = document.createTextNode(": ");
            listElem.appendChild(colon);

            // Value
            let elemValue;
            let type;
            if (data[x].type !== undefined) {
                type = data[x].type;
            } else {
                type = listType;
            }

            if (
                type == NBT.TAG_Compound ||
                type == NBT.TAG_List ||
                type == NBT.TAG_Byte_Array ||
                type == NBT.TAG_Int_Array ||
                type == NBT.TAG_Long_Array
            ) {
                // If the object is a compound, a list or an array:
                // Increment the treeId
                treeId++;

                // Set elemValue to the amount of entries
                elemValue = document.createElement("span");
                elemValue.id = "value_" + treeId;
                for (let i = 0; i < newPath.length; i++) {
                    elemValue.setAttribute("path" + i, newPath[i]);
                }
                elemValue.innerText = Object.keys(getDataFromPath(path.concat([x]))).length + (Object.keys(getDataFromPath(path.concat([x]))).length == 1 ? " entry " : " entries ");

            } else {
                // If the object is not a compound, a list nor an array, set elemValue to data
                elemValue = document.createElement("input");
                if (data[x].data !== undefined) {
                    elemValue.value = data[x].data;
                } else {
                    elemValue.value = data[x];
                }
                elemValue.classList.add("monospace");
                for (let i = 0; i < newPath.length; i++) {
                    elemValue.setAttribute("path" + i, newPath[i]);
                }
                elemValue.addEventListener("change", () => {
                    let i = 0;
                    let path = [];
                    while (elemValue.hasAttribute("path" + i)) {
                        path[i] = elemValue.getAttribute("path" + i);
                        i++;
                    }
                    setDataAtPath(path, elemValue.value);
                    elemValue.value = getDataFromPath(path);
                });
            }
            listElem.appendChild(elemValue);

            // If the object is a compound, a list or an array:
            if (
                type == NBT.TAG_Compound ||
                type == NBT.TAG_List ||
                type == NBT.TAG_Byte_Array ||
                type == NBT.TAG_Int_Array ||
                type == NBT.TAG_Long_Array
            ) {
                // Add a button to expand the tree
                let expandButton = document.createElement("button");
                expandButton.classList.add("monospace");
                expandButton.setAttribute("treeId", treeId);
                for (let i = 0; i < newPath.length; i++) {
                    expandButton.setAttribute("path" + i, newPath[i]);
                }
                let listType = -1;
                if (type == NBT.TAG_List) { listType = data[x].listType; }
                else if (type == NBT.TAG_Byte_Array) { listType = NBT.TAG_Byte; }
                else if (type == NBT.TAG_Int_Array) { listType = NBT.TAG_Int; }
                else if (type == NBT.TAG_Long_Array) { listType = NBT.TAG_Long; }

                expandButton.setAttribute("listType", listType);
                expandButton.addEventListener("click", () => {
                    // If not rendered yet
                    let i = 0;
                    let path = [];
                    while (expandButton.hasAttribute("path" + i)) {
                        path[i] = expandButton.getAttribute("path" + i);
                        i++;
                    }
                    if (!expandButton.hasAttribute("rendered")) {
                        // Render for the first time
                        renderTree(path, document.getElementById("tree_" + expandButton.getAttribute("treeId")), parseInt(expandButton.getAttribute("listType")));
                        expandButton.setAttribute("rendered", "");
                        document.getElementById("refreshBtn_" + expandButton.getAttribute("treeId")).style.display = "inline-block";
                    }

                    if (expandButton.hasAttribute("expanded")) {
                        // Shrink
                        expandButton.removeAttribute("expanded");
                        document.getElementById("tree_" + expandButton.getAttribute("treeId")).style.display = "none";
                        expandButton.innerText = "+";
                    } else {
                        // Expand
                        expandButton.setAttribute("expanded", "");
                        document.getElementById("tree_" + expandButton.getAttribute("treeId")).style.display = "block";
                        expandButton.innerText = "-";
                    }
                });
                expandButton.innerText = "+";
                listElem.appendChild(expandButton);

                // Add a button to refresh the tree
                let refreshButton = document.createElement("button");
                refreshButton.id = "refreshBtn_" + treeId;
                refreshButton.style.display = "none";
                refreshButton.classList.add("monospace");
                refreshButton.setAttribute("treeId", treeId);
                for (let i = 0; i < newPath.length; i++) {
                    refreshButton.setAttribute("path" + i, newPath[i]);
                }
                refreshButton.addEventListener("click", () => {
                    let i = 0;
                    let path = [];
                    while (refreshButton.hasAttribute("path" + i)) {
                        path[i] = refreshButton.getAttribute("path" + i);
                        i++;
                    }
                    renderTree(path, document.getElementById("tree_" + refreshButton.getAttribute("treeId")));
                    document.getElementById("value_" + refreshButton.getAttribute("treeId")).innerText = Object.keys(getDataFromPath(path.concat([x]))).length + (Object.keys(path.concat([x])).length == 1 ? " entry " : " entries ");
                });
                refreshButton.innerText = "Refresh";
                listElem.appendChild(refreshButton);

                // Add an empty div that will contain a subtree
                let subtreeContents = document.createElement("div");
                subtreeContents.id = "tree_" + (treeId);
                listElem.appendChild(subtreeContents);
            }

        list.appendChild(listElem);
    }

    // Append the list to elem
    elem.innerHTML = "";
    elem.appendChild(list);
}

let selectedSlot = -1;
function renderInventory() {
    if (getDataFromPath(["", "Data", "Player", "Inventory"]) === undefined) return;
    while (document.getElementsByClassName("item").length) {
        document.getElementsByClassName("item")[0].remove();
    }
    let data = getDataFromPath(["", "Data", "Player", "Inventory"]);
    for (let i = 0; i < data.length; i++) {
        let itemNode = document.createElement("div");
        itemNode.classList.add("item");
        itemNode.id = "item" + i;
        itemNode.style.backgroundImage = "url(../assets/img/inv/item/" + getItemData(data[i].id.data)[1] + ".png)";
        itemNode.innerText = getItemData(data[i].id.data)[0];
        itemNode.setAttribute("itemid", data[i].id.data);
        itemNode.setAttribute("itemcount", data[i].Count.data);
        itemNode.setAttribute("itemdamage", data[i].Damage.data);
        for (let j of document.getElementsByClassName("slot")) {
            if (j.getAttribute("slotnumber") == data[i].Slot.data) {
                j.appendChild(itemNode);
                break;
            }
        }
    }
}

function selectSlot(slot) {
    selectedSlot = parseInt(slot);
    for (let a of document.getElementsByClassName("slot")) {
        a.classList.remove("selected");
        if (a.getAttribute("slotnumber") == selectedSlot) {
            a.classList.add("selected");
        }
    }
    if (selectedSlot !== -1) {
        document.getElementById("itemDetails").style.display = "block";

        let index;
        for (index = 0; index < getDataFromPath(["", "Data", "Player", "Inventory"]).length; index++) {
            if (getDataFromPath(["", "Data", "Player", "Inventory", index, "Slot"]) == selectedSlot) break;
        }
        let item = getDataFromPath(["", "Data", "Player", "Inventory", index]);
        document.getElementById("itemDetails_id").value = item.id.data;
        document.getElementById("itemDetails_id").children[0].innerHTML = document.getElementById("itemDetails_id").children[1].children[item.id.data].innerHTML;
        document.getElementById("itemDetails_count").value = item.Count.data;
        document.getElementById("itemDetails_damage").value = item.Damage.data;
        document.getElementById("itemDetails_slot").innerText = item.Slot.data;
    } else {
        document.getElementById("itemDetails").style.display = "none";
    }
}

function saveSlot() {
    if (getDataFromPath(["", "Data", "Player", "Inventory"]) === undefined) return;
    let index;
    for (index = 0; index < getDataFromPath(["", "Data", "Player", "Inventory"]).length; index++) {
        if (getDataFromPath(["", "Data", "Player", "Inventory", index, "Slot"]) == selectedSlot) break;
    }

    setDataAtPath(
        ["", "Data", "Player", "Inventory", index, "id"],
        parseInt(document.getElementById("itemDetails_id").getAttribute("value"))
    );
    setDataAtPath(
        ["", "Data", "Player", "Inventory", index, "Damage"],
        parseInt(document.getElementById("itemDetails_damage").value)
    );
    setDataAtPath(
        ["", "Data", "Player", "Inventory", index, "Count"],
        parseInt(document.getElementById("itemDetails_count").value)
    );
    renderInventory();
}

function deleteItem() {
    if (getDataFromPath(["", "Data", "Player", "Inventory"]) === undefined) return;
    for (let i = 0; i < getDataFromPath(["", "Data", "Player", "Inventory"]).length; i++) {
        let x = getDataFromPath(["", "Data", "Player", "Inventory", i]);
        if (x.Slot.data == selectedSlot) {
            setDataAtPath(["", "Data", "Player", "Inventory", i], undefined);
        }
    }
    selectSlot(-1);
    renderInventory();
}



function init() {
    updateMode("main");

    if (defaultTheme == "dark") {
        document.body.classList.add("theme-dark");
    } else {
        document.body.classList.add("theme-light");
    }
    
    for (let x of document.getElementsByTagName("a")) {
        x.link = x.href;
        x.href = "#";
        x.removeEventListener("click", () => {});
        x.addEventListener("click", () => {
            shell.openExternal(x.link);
        });
    }

    for (let x of document.getElementsByClassName("slot")) {
        x.addEventListener("click", () => {
            if (getDataFromPath(["", "Data", "Player", "Inventory"]) === undefined) return;
            if (x.children.length == 0) {
                // Create new item
                if (getDataFromPath(["", "Data", "Player"]).Inventory.listType !== NBT.TAG_Compound) {
                    getDataFromPath(["", "Data", "Player"]).Inventory.listType = NBT.TAG_Compound;
                }
                let path = ["", "Data", "Player", "Inventory"];
                setDataAtPath(path.concat([getDataFromPath(path).length]), {
                    Slot: {data: x.getAttribute("slotnumber"), type: NBT.TAG_Byte},
                    id: {data: 1, type: NBT.TAG_Short},
                    Count: {data: 1, type: NBT.TAG_Byte},
                    Damage: {data: 0, type: NBT.TAG_Short}
                });
                renderInventory();
            }
            selectSlot(x.getAttribute("slotnumber"));
        });
    }

    for (let version of MCVer) {
        let option = document.createElement("option");
        option.value = version;
        option.innerText = version;
        if (version == defaultVersion) {
            option.selected = true;
        }
        document.getElementById("versionSelect").appendChild(option);
    }
    changeVersion(document.getElementById("versionSelect").value);
}
