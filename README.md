# InfEdit

*InfEdit* is a NBT editing tool for old versions of Minecraft (from Minecraft InfDev)

## How to install from source

You need to install NodeJS and then run two commands in the terminal in the project folder:

```
npm install
npm start
```

On Linux you may also have to modify permissions of a file. The path to the file will be shown when the ``npm start`` command will be executed.

```
sudo chown root /path/to/file
sudo chmod 4755 /path/to/file
npm start
```

If you did everything correctly, a window of the aplication should appear.

## How to use

(Work in progress)