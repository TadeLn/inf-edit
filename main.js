const {app, BrowserWindow, Menu, ipcMain, dialog} = require('electron')
let win;

function createWindow () {
    win = new BrowserWindow({
        width: 1920,
        height: 1080,
        icon: "assets/img/icon.png",
        webPreferences: {
            nodeIntegration: true
        }
    });
    //win.webContents.openDevTools();
    win.loadFile("src/index.html");

    let menu = Menu.buildFromTemplate([
        {
            label: "File",
            submenu: [
                {
                    label: "New Window",
                    accelerator: "CmdOrCtrl+N",
                    click() {
                        win.webContents.send("newWindow");
                    }
                },
                {type: "separator"},
                {
                    label: "Select Minecraft Version",
                    accelerator: "CmdOrCtrl+Shift+V",
                    click() {
                        win.webContents.send("changeVersion");
                    }
                },
                {type: "separator"},
                {
                    label: "Load",
                    accelerator: "CmdOrCtrl+O",
                    click() {
                        win.webContents.send("loadFile_start");
                    }
                },
                {
                    label: "Reload",
                    accelerator: "CmdOrCtrl+R",
                    click() {
                        win.webContents.send("load_start");
                    }
                },
                {
                    label: "Save",
                    accelerator: "CmdOrCtrl+S",
                    disabled: true,
                    click() {
                        win.webContents.send("save_start");
                    }
                },
                {
                    label: "Save As...",
                    accelerator: "CmdOrCtrl+Shift+S",
                    disabled: true,
                    click() {
                        win.webContents.send("saveAs_start");
                    }
                },
                {type: "separator"},
                {
                    label: "Export...",
                    submenu: [
                        {
                            label: "Export as JSON",
                            accelerator: "CmdOrCtrl+E",
                            click() {
                                win.webContents.send("exportFile_start", 1);
                            }
                        },
                        {
                            label: "Export as JSON without types",
                            click() {
                                win.webContents.send("exportFile_start", 2);
                            }
                        },
                        {
                            label: "Export as compressed NBT",
                            click() {
                                win.webContents.send("exportFile_start", 3);
                            }
                        },
                        {
                            label: "Export as uncompressed NBT",
                            click() {
                                win.webContents.send("exportFile_start", 4);
                            }
                        }
                    ]
                },
                {type: "separator"},
                {
                    label: "Exit",
                    accelerator: "CmdOrCtrl+Q",
                    click() {
                        app.quit();
                    }
                }
            ],
        },
        {
            label: "View",
            submenu: [
                {
                    label: "Toggle Fullscreen",
                    accelerator: "F11",
                    click() {
                        win.webContents.send("toggleFullscreen");
                    }
                },
                {
                    label: "Zoom",
                    submenu: [
                        {
                            label: "50%",
                            click() {
                                win.webContents.send("zoom", 0.5);
                            }
                        },
                        {
                            label: "75%",
                            click() {
                                win.webContents.send("zoom", 0.75);
                            }
                        },
                        {
                            label: "100%",
                            click() {
                                win.webContents.send("zoom", 1);
                            }
                        },
                        {
                            label: "150%",
                            click() {
                                win.webContents.send("zoom", 1.5);
                            }
                        },
                        {
                            label: "200%",
                            click() {
                                win.webContents.send("zoom", 2);
                            }
                        }
                    ]
                }
            ]
        },
        {
            label: "Mode",
            submenu: [
                {
                    label: "Main Page",
                    accelerator: "F1",
                    click() {
                        win.webContents.send("changeMode", "main");
                    }
                },
                {
                    label: "Tree View",
                    accelerator: "F2",
                    click() {
                        win.webContents.send("changeMode", "tree");
                    }
                },
                {
                    label: "Edit Inventory",
                    accelerator: "F3",
                    click() {
                        win.webContents.send("changeMode", "inventory");
                    }
                },
                {
                    label: "Edit World",
                    accelerator: "F4",
                    click() {
                        win.webContents.send("changeMode", "world");
                    }
                },
                {
                    label: "Edit World Settings",
                    accelerator: "F5",
                    click() {
                        win.webContents.send("changeMode", "worldSettings");
                    }
                }
            ]
        },
        {
            label: "About",
            submenu: [
                {
                    label: "Help",
                    accelerator: "Shift+F1",
                    click() {
                        win.webContents.send("changeMode", "help");
                    }
                },
                {
                    label: "Credits",
                    click() {
                        win.webContents.send("changeMode", "credits");
                    }
                }
            ],
        },
        {
            label: "Debug",
            submenu: [
                {
                    label: "Open DevTools",
                    accelerator: "F12",
                    click() {
                        win.webContents.openDevTools();
                    }
                }
            ],
        }
    ]);

    Menu.setApplicationMenu(menu);
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
    }
})



ipcMain.on("loadFileDialog_start", (event) => {
    dialog.showOpenDialog({
        properties: ["openFile", "showHiddenFiles"],
        filters: [
            { name: "All supported formats", extensions: ["nbt", "dat", "nbt.uncompressed", "dat.uncompressed"] },
            { name: "DAT files", extensions: ["dat", "dat.uncompressed"] },
            { name: "NBT files", extensions: ["nbt", "nbt.uncompressed"] },
            { name: "All Files", extensions: ["*"] }
        ]
    }).then(files => {
        if (files) {
            event.reply("loadFileDialog_return", (!files.canceled && files.filePaths !== undefined && files.filePaths.length > 0) ? files.filePaths[0] : "");
        }
    })
});

ipcMain.on("saveFileDialog_start", (event) => {
    dialog.showSaveDialog({
        properties: ["showHiddenFiles"],
        filters: [
            { name: "All supported formats", extensions: ["nbt", "dat", "nbt.uncompressed", "dat.uncompressed"] },
            { name: "DAT files", extensions: ["dat", "dat.uncompressed"] },
            { name: "NBT files", extensions: ["nbt", "nbt.uncompressed"] },
            { name: "All Files", extensions: ["*"] }
        ]
    }).then(file => {
        if (file) {
            event.reply("saveFileDialog_return", (!file.canceled && file.filePath !== undefined) ? file.filePath : "");
        }
    });
});

ipcMain.on("exportFileDialog_start", (event, type) => {
    dialog.showSaveDialog({
        title: "Export...",
        properties: ["showHiddenFiles"],
        buttonLabel: "Export",
        filters: [
            { name: "JSON File", extensions: ["json"] },
            { name: "All Files", extensions: ["*"] }
        ]
    }).then(file => {
        if (file) {
            event.reply("exportFileDialog_return", (!file.canceled && file.filePath !== undefined) ? file.filePath : "", type);
        }
    })
});